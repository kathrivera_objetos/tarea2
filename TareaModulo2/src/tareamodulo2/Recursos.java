package tareamodulo2;
public class Recursos {
    //Primer Metodo
    public void mostrarMensaje(){
        javax.swing.JOptionPane.showMessageDialog(null, "Estoy aprendiendo, pero sere la mejor programadora");
    }
    
    //Segundo Metodo
    public String esMayor(float edad){
        return (edad>=18)?"Es Mayor de Edad":"Es Menor de Edad";
    }
    
    //Tercer Metodo
    public int multiplicacion(int v1, int v2){
        return(v1*v2);
    }
    
    //Cuarto Metodo
    
    public String listaNumeros(int x){
        String cadena="";
        for(int i=1;i<=x;i++){
            cadena+=i+", ";
        }
        return cadena;
    }
}
