package tareamodulo2;
import javax.swing.JOptionPane;
public class Principal {
    public static void main(String args[]){
        Recursos rec = new Recursos();
        
        //imprimir primer metodo
        rec.mostrarMensaje();
        
        //imprimir segundo metodo
        float edad = Float.parseFloat(JOptionPane.showInputDialog("Ingrese la edad del Estudiante"));
        JOptionPane.showMessageDialog(null,"La edad del estudiante es: "+edad+"\nEl Estudiante "+rec.esMayor(edad));
        
        //Imprimir tercer Metodo
        int num1 = Integer.parseInt(JOptionPane.showInputDialog("Ingrese el primer Numero"));
        int num2 = Integer.parseInt(JOptionPane.showInputDialog("Ingrese el segundo Numero"));
        JOptionPane.showMessageDialog(null,"La multiplicacion de "+num1+" y "+num2+" es: "+ rec.multiplicacion(num1, num2));
        
        //imprimir cuarto Metodo
        int x = Integer.parseInt(JOptionPane.showInputDialog("Ingrese el a listar"));
        JOptionPane.showMessageDialog(null,rec.listaNumeros(x));
    }
}
